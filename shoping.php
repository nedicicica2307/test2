<?php
if(!isset($_SESSION)) session_start(); 
require_once 'DAO.php';
$msg = isset($msg)?$msg:'';
$dao = new DAO();
$articles = $dao->selectArticles();

?>
        <!DOCTYPE html>
        <html lang="en">

        <head>
            <meta charset="UTF-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>USER</title>
            <style>
            body{
            background-color: #f5e8d7;
            }
            h1{
            color: darkslateblue;
            text-align: center;
            font-style: italic;
            font-weight: bold;
            font-family: sans-serif;
            letter-spacing: 10px;
            }
           
            a{
            text-transform: uppercase;
            font-size: 14px;
            
            }
            table{
            border: 2px;
            border-color: black;
            border-style: solid;
            padding: 3px;
            margin-bottom: 10px;
            text-align: center;
        }
        th{
            font-family: Arial, sans-serif;
            font-size: 16px; 
            text-decoration: underline;
        }
        td{
            font-family: Arial, sans-serif;
            font-size: 16px; 
            border-top: 1px solid;
            border-bottom: 1px solid;
            padding: 8px;
        }
            </style>
        </head>

        <body>
            <h1>SHOP</h1>
            <table>
                <tr>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Type</th>
                    <th>Action</th>
                </tr>
                <?php foreach($articles as $pom){?>
                <tr>
                    <td><?= $pom['name'] ?></td>
                    <td><?= $pom['price'] ?></td>
                    <td><?= $pom['article_type'] ?></td>
                    <td><a href='CartController.php?action=addToCart&article=<?= serialize ($pom)?>'>ADD TO CART</a>  </td>
                </tr>
                <?php }?>
            </table>
            <br>
            <?= $msg ?>
            <br>
            <a href="cart.php">CART</a><br>
            <a href="logout.php">LOGOUT</a>
        </body>
        </html>
