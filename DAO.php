<?php
require_once 'db.php';

class DAO {
	private $db;

	
	private $SELECT_USER = "SELECT * FROM users";
	private $SELECT_USER_BY_USERNAME_AND_PASSWORD = "SELECT * FROM users JOIN users_atributes ON users.id_user_atributes = users_atributes.id_user_atribute where users.username = ? and users.password = ?";
	private $INSERT_USER = "INSERT INTO users (type, username, password, id_user_atributes) VALUES (?, ?, ?, ?)";
	private $INSERT_USER_ATRIBUTES = "INSERT INTO users_atributes (name, lastname, email) VALUES (?, ?, ?)";
	private $DELETE_ARTICLE_BY_ID_ARTICLE = "DELETE FROM articles WHERE id_article = ?";
	private $DELETE_USER_BY_ID_USER = "DELETE FROM users WHERE id_user = ?";
	private $SELECT_ARTICLES = "SELECT * FROM articles";
	private $SELECT_USER_BY_ID_USER = "SELECT users_atributes.name FROM users_atributes JOIN users ON users.id_user_atributes = users_atributes.id_user_atribute WHERE id_user = ?";
	private $SELECT_ARTICLE_BY_ID  = "SELECT * FROM `articles` WHERE id_article = ?";
	private $SELECT_ALL = "SELECT users_atributes.name, users_atributes.lastname, users_atributes.email, users.type, users.username, users.password, users.id_user FROM users_atributes JOIN users ON users_atributes.id_user_atribute=users.id_user_atributes ";
	private $INSERT_ARTICLE_TYPE = "INSERT INTO article_types (article_type) VALUES (?)";
	private $SELECT_ARTICLES_AND_TYPE = "SELECT articles.name, articles.price, articles.article_type FROM articles JOIN article_types ON articles.article_type = article_types.article_type WHERE articles.name = ? AND articles.price = ?";
	private $SELECT_ARTICLE_TYPES = "SELECT * FROM article_types";
	private $INSERT_ARTICLES = "INSERT INTO articles (name, price,article_type) VALUES (?, ?, ?) ";
	private $UPDATE_ARTICLE = "UPDATE osobe SET name= ?, price= ?, article_type= ? WHERE id_article=?";
	private $INSERT_LOGS = "INSERT INTO logs (id_user, action, pages, pages_item) VALUES (?, ?, ?, ?) ";
	private $SELECT_LOGS = "SELECT * FROM logs";
	private $INSERT_ORDER = "INSERT INTO orders (customer_name, price,order_date) VALUES (?, ?, ?)";
	private $SELECT_USER_TYPE = "SELECT type FROM users WHERE id_user = ?";
	
	public function __construct()
	{
		$this->db = DB::createInstance();
	
	}
	
	public function selectUserType($id_user)
	{
		$statement = $this->db->prepare($this->SELECT_USER_TYPE);
		$statement->bindValue(1, $id_user);
		$statement->execute();
		
		$result = $statement->fetchAll();
		return $result;
	
	}

	public function selectUser()
	{
		$statement = $this->db->prepare($this->SELECT_USER);
		$statement->execute();
		
		$result = $statement->fetchAll();
		return $result;
	
	}

	public function selectAll()
	{
		$statement = $this->db->prepare($this->SELECT_ALL);
		$statement->execute();
		
		$result = $statement->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	
	}

	public function selectUserFirstNameById($id_user)
	{
		$statement = $this->db->prepare($this->SELECT_USER_BY_ID_USER);
		$statement->bindValue(1, $id_user);
		$statement->execute();
		
		$result = $statement->fetchColumn();
		return $result;
	
	}

	public function selectArticlesAndType($name, $price)
	{
		$statement = $this->db->prepare($this->SELECT_ARTICLES_AND_TYPE);
		$statement->bindValue(1, $name);
		$statement->bindValue(2, $price);
		$statement->execute();

		$result = $statement->fetch();
		return $result;
	
	}

	public function selectUserByUsernameAndPassword($username, $password)
	{
		
		$statement = $this->db->prepare($this->SELECT_USER_BY_USERNAME_AND_PASSWORD);
		$statement->bindValue(1, $username);
		$statement->bindValue(2, $password);
		$statement->execute();

		$result = $statement->fetch();
		return $result;
	
	}

	public function selectArticleById($id_article)
	{
		$statement = $this->db->prepare($this->SELECT_ARTICLE_BY_ID);
		$statement->bindValue(1, $id_article);
		$statement->execute();

		$result = $statement->fetch();
		return $result;
	
	}

	 function insertUserAtributes($name, $lastname, $email)
	{
		$statement = $this->db->prepare($this->INSERT_USER_ATRIBUTES);
		$statement->bindValue(1, $name);
		$statement->bindValue(2, $lastname);
		$statement->bindValue(3, $email);
		
		$statement->execute();
		return $this->db->lastInsertID();
	
	}

	public function insertUser($type, $username, $password, $id_user_atributes)
	{
		$statement = $this->db->prepare($this->INSERT_USER);
		$statement->bindValue(1, $type);
		$statement->bindValue(2, $username);
		$statement->bindValue(3, $password);
		$statement->bindValue(4, $id_user_atributes);
		
		$statement->execute();

	}
	public function deleteArticleByIdArticle($id_article)
	{
		$statement = $this->db->prepare($this->DELETE_ARTICLE_BY_ID_ARTICLE);
		$statement->bindValue(1, $id_article);
		
		$statement->execute();
		
	}

	public function deleteUserByIdUser($id_user)
	{
		$statement = $this->db->prepare($this->DELETE_USER_BY_ID_USER);
		$statement->bindValue(1, $id_user);
		
		$statement->execute();
		
	}

	public function selectArticles()
	{
		$statement = $this->db->prepare($this->SELECT_ARTICLES);
		
		$statement->execute();
		
		$result = $statement->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	
	}

	public function selectArticleTypes()
	{
		$statement = $this->db->prepare($this->SELECT_ARTICLE_TYPES);
		
		$statement->execute();
		
		$result = $statement->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	
	}

	public function insertArticleType($article_type)
	{
		$statement = $this->db->prepare($this->INSERT_ARTICLE_TYPE);
		$statement->bindValue(1, $article_type);

		
		$statement->execute();
		return $this->db->lastInsertID();
		
	}
	
	public function insertArticles($name, $price,$article_type)
	{
		$statement = $this->db->prepare($this->INSERT_ARTICLES);
		$statement->bindValue(1, $name);
		$statement->bindValue(2, $price);
		$statement->bindValue(3, $article_type);
		
		$statement->execute();
		
	}

	public function insertLogs($id_user, $action, $pages, $pages_items)
	{
		$statement = $this->db->prepare($this->INSERT_LOGS);
		$statement->bindValue(1, $id_user);
		$statement->bindValue(2, $action);
		$statement->bindValue(3, $pages);
		$statement->bindValue(4, $pages_items);

		$statement->execute();

	}

	public function selectLogs()
	{
		$statement = $this->db->prepare($this->SELECT_LOGS);
		
		$statement->execute();
		
		$result = $statement->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	
	}

	public function insertOrder($customer_name, $price, $order_date)
	{
		$statement = $this->db->prepare($this->INSERT_ORDER);
		$statement->bindValue(1, $customer_name);
		$statement->bindValue(2, $price);
		$statement->bindValue(3, $order_date);

		$statement->execute();

	}

	public function updateArticle($id_article, $name, $price, $article_type)
	{
		$statement = $this->db->prepare($this->UPDATE_ARTICLE);
		$statement->bindValue(1, $id_article);
		$statement->bindValue(2, $name);
		$statement->bindValue(3, $price);
		$statement->bindValue(4, $article_type);
		
		$statement->execute();
		
	}
}
?>