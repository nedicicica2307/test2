<?php
require_once 'DAO.php';

$action = isset($_REQUEST["action"])? $_REQUEST["action"] : ""; 
$article_type= isset($_POST['article_type'])? test_input($_POST['article_type']) : "";
$customer_name= isset($_GET['customer_name'])? test_input($_GET['customer_name']) : "";
$price= isset($_GET['price'])? test_input($_GET['price']) : "";
$order_date= isset($_GET['order_date'])? test_input($_GET['order_date']) : "";

if ($_SERVER['REQUEST_METHOD']=="GET"){
    
    if ($action == 'addToCart') {
        $article = isset($_GET["article"])? unserialize($_GET["article"]) : "";
        if(!isset($_SESSION)) session_start(); 
        if(!isset($_SESSION['cart'])) $_SESSION['cart']=[]; 
         $_SESSION['cart'][] = $article;
        $msg = 'Article succesfully added !';
        include_once 'shoping.php';
        die();

    } elseif ($action == 'removeFromCart') {
        $article = isset($_GET["article"])? unserialize($_GET["article"]) : ""; 
        if(!isset($_SESSION)) session_start(); 
        if(!isset($_SESSION['cart'])) $_SESSION['cart']=[]; 
 
        if (($key = array_search($article, $_SESSION['cart'])) !== false) {
            unset($_SESSION['cart'][$key]);
        }
        $msg = 'Article succesfully deleted!!!';
        include_once 'cart.php';

    } elseif ($action == 'emptyCart') {
        if(!isset($_SESSION)) session_start(); 
        unset($_SESSION['cart']);
        $msg = 'Cart is empty!';
        header('Location: cart.php');

    } elseif ($action == 'confirmShoping') {
        unset($_SESSION['cart']);
        $_SESSION['confirmShoping'] = true;
        $dao = new DAO();
        $orders = [];
        $orders = $dao->insertOrder($customer_name, $price, $order_date);
        header('location:orders.php');
  
    } elseif ($action == 'showArticleTypes') {
        $dao = new DAO();
        $article_types = $dao->selectArticleTypes();
        include_once 'articleTypeList.php';

    } elseif ($action == 'showArticles') {
        $dao = new DAO();
        $articles = $dao->selectArticles();
        include_once 'articleList.php';

    } elseif ($action == 'delete') {
        $dao = new DAO();
        $userID = $_COOKIE['loggedUser'];
        $dao->deleteArticleByIdArticle($_REQUEST['id_article']);
        $articles = $dao->selectArticles();
        $dao->insertLogs($userID, "delete", "article" ,$_REQUEST['id_article']);
        include ('articleList.php');
      
    } elseif ($action == 'edit') {
        $id_article = isset($_GET['id_article'])?$_GET['id_article'] : '';
        $name = isset($_GET['name'])?$_GET['name'] : '';
        $price = isset($_GET['price'])?$_GET['price'] : '';
        $article_type = isset($_GET['article_type'])?$_GET['article_type'] : '';
        $dao = new DAO();
        $articles = $dao->selectArticlesAndType($id_article, $name, $price, $article_type);
        include ('updateArticle.php');

    } else {
        echo 'Error wrong GET action';
    }
    
} elseif ($_SERVER['REQUEST_METHOD']=="POST"){
   
    if ($action == 'addArticleType') {  
        $dao = new DAO();
        $article_type = '';
        $article_type = $dao->insertArticleType($article_type);
        include 'articleTypes.php';
         
    } elseif ($action == 'addNewArticle'){
        $name = isset($name)?$name: '';
        $price = isset($price)?$price: '';
        $article_type = isset($article_type)?$article_type: '';
        $dao = new DAO();
        
       
        $dao->insertArticleType($article_type);
        $articles[] = $dao->insertArticles($name,$price,$article_type);
        $dao->selectArticlesAndType($name,$price);
        include 'articles.php';
        
    }elseif ($action == 'update'){
        $dao = new DAO();
       $dao->insertArticles($name, $price,$article_type);
       $osobe = $dao->updateArticle($id_articles, $name, $price,$article_type);
       $osobe = $dao->selectArticles();
       include_once 'articles.php';
    } else {
        echo 'Error wrong POST action';
    }
    
} else {
    header("Location: home.php");
    die();
}


function test_input($data){
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>