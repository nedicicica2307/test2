<?php
require_once 'DAO.php';
$dao = new DAO();
//$userID = '';
//$user = [];

// Provera da li postoji kolačić sa nazivom "moj_kolacic"
if(isset($_COOKIE['loggedUser'])) {
    // Povlačenje vrednosti kolačića
    $userID = $_COOKIE['loggedUser'];
    $userFirstName = $dao->selectUserFirstNameById($userID);
} else {
    // Dok kolacic nije ucitan
    $userFirstName = $dao->selectUserFirstNameById($user['id_user']);
}


$dao->selectUserType($user['id_user']);
$userType = $dao->selectUserType($user['id_user'])[0]['type'];

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        body{
            background-color: #f5e8d7;
        }
        h1{
            color: darkslateblue;
            text-align: center;
            font-style: italic;
            font-weight: bold;
            font-family: sans-serif;
            letter-spacing: 10px;
        }
        ul{
            list-style-type: none;
            overflow: hidden;
        }
        li a{
            text-align: center;
            color:darkblue;
            padding: 10px 15px;
        }
    </style>
</head>
<body>
<h1>Dobrodosli, <?php echo $userFirstName;  ?></h1>
<ul>
<?php if($userType == "administrator"){ ?>
        <li><a href="users.php">Users</a></li>
        <li><a href="logs.php">Logs</a></li>
        <li> <a href="articleTypes.php">Article types</a> </li>
        <li><a href="articles.php">Articles</a></li>
    <?php }elseif($userType == "moderator"){ ?>
    
    <li> <a href="articleTypes.php">Article types</a> </li>
    <li><a href="articles.php">Articles</a></li>
    
    <?php }elseif($userType == "user") { ?>
       <li><a href="shoping.php">Shopping</a><br></li>
    <?php } ?>
    

   

    <li> <a href="logout.php">Logout</a> </li>
</ul>

    
</body>
</html>