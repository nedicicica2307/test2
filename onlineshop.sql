-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 11, 2023 at 10:33 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `onlineshop`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id_article` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` float NOT NULL,
  `article_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id_article`, `name`, `price`, `article_type`) VALUES
(3, 'jaffa', 150, 'keks'),
(4, 'domacica', 200, 'keks'),
(17, '', 0, 'jogurt'),
(21, '', 0, 'sss');

-- --------------------------------------------------------

--
-- Table structure for table `article_types`
--

CREATE TABLE `article_types` (
  `article_type` varchar(255) NOT NULL,
  `id_article_type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `article_types`
--

INSERT INTO `article_types` (`article_type`, `id_article_type`) VALUES
('asd', 2),
('asdasd', 3),
('cokolada', 4),
('keks', 5),
('mleko', 6),
('sok', 7),
('xcvxcv', 8),
('uuyt', 22),
('cdd', 23),
('', 24),
('jogurt', 25),
('tt', 26),
('rrr', 27),
('qq', 28),
('sss', 29);

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `action` varchar(255) NOT NULL,
  `pages` varchar(255) NOT NULL,
  `pages_item` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `id_user`, `action`, `pages`, `pages_item`) VALUES
(1, 2, 'delete', 'articles', 3),
(2, 2, 'delete', 'articles', 3),
(3, 1, 'delete', 'articles', 10),
(4, 1, 'delete', 'articles', 12),
(5, 2, 'delete', 'user', 14),
(6, 2, 'delete', 'article', 13),
(7, 2, 'delete', 'article', 13),
(8, 2, 'delete', 'article', 13),
(9, 2, 'delete', 'article', 13),
(10, 2, 'delete', 'article', 16),
(11, 2, 'delete', 'article', 15),
(12, 2, 'delete', 'article', 15),
(13, 2, 'delete', 'article', 19),
(14, 2, 'delete', 'article', 18),
(15, 2, 'delete', 'article', 20),
(16, 2, 'delete', 'article', 20),
(17, 16, 'delete', 'user', 1),
(18, 13, 'delete', 'user', 16);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL,
  `customer_name` varchar(255) NOT NULL,
  `price` float NOT NULL,
  `order_date` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `customer_name`, `price`, `order_date`) VALUES
(1, '', 0, '0000-00-00'),
(2, '', 0, '0000-00-00'),
(3, '', 0, '0000-00-00'),
(4, '', 0, '0000-00-00'),
(5, '', 0, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `id_user_atributes` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_user`, `type`, `username`, `password`, `id_user_atributes`) VALUES
(2, 'user', 'jovan', 'jovan', 2),
(11, 'user', 'dusko', 'dusko', 13),
(12, 'moderator', 'milica', 'milica', 15),
(13, 'administrator', 'simo', 'simo', 16),
(17, 'moderator', 'aa', 'aa', 20);

-- --------------------------------------------------------

--
-- Table structure for table `users_atributes`
--

CREATE TABLE `users_atributes` (
  `id_user_atribute` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users_atributes`
--

INSERT INTO `users_atributes` (`id_user_atribute`, `name`, `lastname`, `email`) VALUES
(1, 'Marko', 'Markovic', 'marko2020@gmail.com'),
(2, 'Jovan', 'Jovanovic', 'jovan123@hotmail.com'),
(3, 'Pero', 'Peric', 'pp@dd.com'),
(4, 'Pero', 'Peric', 'ttt@ff.rs'),
(5, 'Pero', 'Peric', 'ttt@ff.rs'),
(6, 'Pero', 'Peric', 'ttt@ff.rs'),
(7, 'Neda', 'Lazovic', 'nedicicica@hotmail.com'),
(8, 'Neda', 'Lazovic', 'nedicicica@hotmail.com'),
(9, 'Pero', 'Peric', 'ss@dd.com'),
(10, 'Pero', 'Peric', 'dd@ss.pd'),
(11, 'Pero', 'Peric', 'dd@ss.pd'),
(12, 'Neda', 'Lazovic', 'nedicicica@hotmail.com'),
(13, 'Dusko', 'Lazovic', 'dded@dd.kk'),
(14, 'Milica', 'Milic', 'miim@dd.rs'),
(15, 'Milica', 'Milic', 'miim@dd.rs'),
(16, 'Simo', 'Simic', 'bb@ww.cc'),
(17, 'yy', 'gg', 'gg@tf.jj'),
(18, 'vv', 'vv', 'vv@ss.kh'),
(19, 'djordje', 'djordjevic', 'dd@se.com'),
(20, 'aa', 'ss', 'ss@ss.vv');

-- --------------------------------------------------------

--
-- Table structure for table `user_atributes_and_types`
--

CREATE TABLE `user_atributes_and_types` (
  `id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `user_types`
--

CREATE TABLE `user_types` (
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_types`
--

INSERT INTO `user_types` (`type`) VALUES
('administrator'),
('moderator'),
('user');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id_article`),
  ADD KEY `article_type` (`article_type`);

--
-- Indexes for table `article_types`
--
ALTER TABLE `article_types`
  ADD PRIMARY KEY (`article_type`),
  ADD UNIQUE KEY `id_article_type` (`id_article_type`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`),
  ADD KEY `type` (`type`),
  ADD KEY `username` (`username`),
  ADD KEY `id_user_atributes` (`id_user_atributes`);

--
-- Indexes for table `users_atributes`
--
ALTER TABLE `users_atributes`
  ADD PRIMARY KEY (`id_user_atribute`);

--
-- Indexes for table `user_atributes_and_types`
--
ALTER TABLE `user_atributes_and_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_types`
--
ALTER TABLE `user_types`
  ADD PRIMARY KEY (`type`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id_article` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `article_types`
--
ALTER TABLE `article_types`
  MODIFY `id_article_type` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `users_atributes`
--
ALTER TABLE `users_atributes`
  MODIFY `id_user_atribute` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `user_atributes_and_types`
--
ALTER TABLE `user_atributes_and_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `articles`
--
ALTER TABLE `articles`
  ADD CONSTRAINT `articles_ibfk_1` FOREIGN KEY (`article_type`) REFERENCES `article_types` (`article_type`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`type`) REFERENCES `user_types` (`type`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `users_ibfk_2` FOREIGN KEY (`id_user_atributes`) REFERENCES `users_atributes` (`id_user_atribute`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
