<?php
$msg = isset($msg) ? $msg: "";
if(!isset($_SESSION)) session_start(); 
$articles = isset($_SESSION['cart'])?$_SESSION['cart']:[];
?>
<!DOCTYPE html>
     <html lang="en">
         <head>
            <meta charset="UTF-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>SHOP</title>
            <style>
                 body{
            background-color: #f5e8d7;
            }
            h2{
            color: darkslateblue;
            text-align: center;
            font-style: italic;
            font-weight: bold;
            font-family: sans-serif;
            letter-spacing: 10px;
            }
            </style>
        </head>

        <body>
            <h2>ARTICLES</h2>
            <?php if(count($articles)>0){ ?>
            <table border="1px;" align="center" width="60%">
                <tr>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Type</th>
                    <th>Action</th>
                </tr>
                <?php foreach($articles as $pom){?>
                <tr>
                    <td><?= $pom['name'] ?></td>
                    <td><?= $pom['price'] ?></td>
                    <td><?= $pom['article_type'] ?></td>
                    <td><a href='cartController.php?action=removeFromCart&article=<?= serialize ($pom)?>'>REMOVE FROM CART</a></td>
                </tr>
                <?php }?>
            </table>
            <?php } else { ?>
                <h2>Cart is empty</h2>
            <?php }  ?>
            <br>
            <?= $msg ?>
            <br>
            <a href='cartController.php?action=emptyCart'>EMPTY CART</a><br>             
            <a href="shoping.php">CONTINUE SHOPING</a><br>
            <a href='cartController.php?action=confirmShoping'>CONFIRM SHOPING</a><br> 
            <a href="logout.php">LOGOUT</a><br>
        </body>

        </html>   


