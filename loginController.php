<?php
require_once 'DAO.php';

    $action = isset($_REQUEST["action"])? $_REQUEST["action"] : ""; 

    $type = isset($_GET['type'])? test_input($_GET['type']) : '';
    $name = isset($_GET['name'])? test_input($_GET['name']) : '';
    $lastname = isset($_GET['lastname'])? test_input($_GET['lastname']) : '';
    $email = isset($_GET['email'])? test_input($_GET['email']) : '';
    $id_user= isset($_GET['id_user'])? test_input($_GET['id_user']) : '';
    $username = isset($_POST['username'])? test_input($_POST['username']) : '';
    $password = isset($_POST['password'])? test_input($_POST['password']) : '';
    $id_user = isset($id_user)?$id_user : '';

    if ($_SERVER['REQUEST_METHOD']=="GET"){
   
    if ($action == 'delete') {
     
        $dao = new DAO();
        $userID = $_COOKIE['loggedUser'];
        $dao->deleteUserByIdUser($id_user);
        $users = $dao->selectAll();
        $dao->insertLogs($userID, "delete", "user" ,$_REQUEST['id_user']);
        include ('usersTable.php');

    } elseif ($action == 'showAll') {
        
        $dao = new DAO();
        $users = $dao->selectAll();
        include_once 'usersTable.php';
    
    }else {
        echo 'Error wrong GET action';
    }

    } elseif ($_SERVER['REQUEST_METHOD']=="POST"){
   
    if ($action == 'login') {
        
        $dao = new DAO();
        $user= $dao->selectUserByUsernameAndPassword($username,$password);
        if($user){

            $cookie_value = $user['id_user'];
            $expiration_time = time() + 24 * 60 * 60;
            setcookie('loggedUser', $cookie_value, $expiration_time, '/');
            include_once 'home.php';
        }else{
            $msg = "Wrong login parametars!";
            include_once 'login.php';
        }
        
        }elseif ($action == 'register'){
        $name = isset($_POST['name'])? test_input($_POST['name']) : '';
        $lastname = isset($_POST['lastname'])? test_input($_POST['lastname']) : '';
        $email = isset($_POST['email'])? test_input($_POST['email']) : '';
        $type = isset($_POST['type'])? test_input($_POST['type']) : '';
        $username = isset($_POST['username'])? test_input($_POST['username']) : '';
        $password = isset($_POST['password'])? test_input($_POST['password']) : '';
        $repeatPassword = isset($_POST["repeatPassword"])? test_input($_POST["repeatPassword"]) : ''; 

        if(!($name == "" || $lastname == "" || $email == "" || $username == "" || $password == "" || $repeatPassword == "" )){
            if($password == $repeatPassword){ 
                $dao = new DAO();
                $id_user_atributes = $dao->insertUserAtributes($name, $lastname, $email);
                $dao->insertUser($type, $username,$password, $id_user_atributes);
                  
                include_once 'users.php';

                
            }else{
                $msg = "Passwords must be the same!";
                include_once 'users.php';
            }

        }else{
        $msg = "All inputs must be filled!";
        include_once 'users.php';
    }
}
}else {
    header("Location: login.php"); 
    die();
}


function test_input($data){
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>