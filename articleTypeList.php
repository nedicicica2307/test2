<?php
require_once 'DAO.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
          body{
            background-color: #f5e8d7;
        }
        h1{
            color: darkslateblue;
            text-align: center;
            font-style: italic;
            font-weight: bold;
            font-family: sans-serif;
            letter-spacing: 10px;
        }
        
        li{
            color:#524d3f;
            margin-bottom: 10px;

        }
    </style>
</head>
<body>
    <h1>Article type list</h1>
    <ol>
        <?php foreach($article_types as $pom){  ?>
        <li><?= $pom['article_type'] ?></li>
        <?php  }?>
    </ol>
    <a href="home.php">Home</a>
</body>
</html>