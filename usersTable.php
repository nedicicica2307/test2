
<?php
if(!isset($_SESSION)) session_start(); 
require_once 'DAO.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        body{
            background-color: #f5e8d7;
        }
        h1{
            color: darkslateblue;
            text-align: center;
            font-style: italic;
            font-weight: bold;
            font-family: sans-serif;
            letter-spacing: 10px;
        }
        table{
            border: 2px;
            border-color: black;
            border-style: solid;
            padding: 3px;
            margin-bottom: 10px;
        }
        th{
            font-family: Arial, sans-serif;
            font-size: 16px; 
            text-decoration: underline;
        }
        td{
            font-family: Arial, sans-serif;
            font-size: 16px; 
            border-top: 1px solid;
            border-bottom: 1px solid;
            padding: 8px;
        }
        a{
            text-transform: uppercase;
            font-size: 14px;
        }
    </style>
</head>
<body>
<h1>User table</h1>
    
    <table >
        <tr>
            <th>Type</th>
            <th>Name</th>
            <th>Lastname</th>
            <th>Email</th>
            <th>Username</th>
            <th>Password</th>
            <th >Edit</th>
            <th >Delete</th>
        </tr>
        <?php foreach($users as $pom){?>
        <tr>
            <td><?=$pom['type'] ?></td>
            <td><?=$pom['name'] ?></td>
            <td><?=$pom['lastname'] ?></td>
            <td><?=$pom['email'] ?></td>
            <td><?=$pom['username'] ?></td>
            <td><?=$pom['password'] ?></td>
            <td><a href="loginController.php?action=edit&id_user=<?=$pom['id_user'] ?>">Edit</a></td>
            <td><a href="loginController.php?action=delete&id_user=<?=$pom['id_user'] ?>">Delete</a></td>
        </tr>
        <?php }?>
        
    </table>
<a href="home.php">Home</a>
</body>
</html>